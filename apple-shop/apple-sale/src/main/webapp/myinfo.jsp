<%@ page language="java" contentType="text/html; charset=utf-8"
		 pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Single &mdash; A free HTML5 Template </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap Style -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Theme Style -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<script src="js/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" type="text/css"
		  href="styles/bootstrap-4.1.3/bootstrap.css">
	<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css"
		  rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css"
		  href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
	<link rel="stylesheet" type="text/css"
		  href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
	<link rel="stylesheet" type="text/css"
		  href="plugins/OwlCarousel2-2.2.1/animate.css">
	<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
	<link rel="stylesheet" type="text/css" href="styles/responsive.css">
	<script src="js/jquery-3.2.1.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<div id="fh5co-bio">
		<div id="dd_div">
			<!--异步加载其他文件-->
			<script src="https://code.jquery.com/jquery-3.1.1.min.js">
                $("#dd_div").load("sidebar.jsp");
			</script>

		</div>

	</div>
		<div id="fh5co-portrait" style="background-image: url(images/hero.jpg);" data-minheight="500">
			<div id="fh5co-toggle" class="js-fh5co-toggle to-animate-single"><a href="#fh5co-bio" class="fh5co-active"><i class="icon-align-right"></i></a></div>
			<div id="fh5co-intro">
				<h1><span class="to-animate">欢迎你，${customer.aname}</span> <span class="to-animate">这是你的主页,</span> <span class="to-animate">稍后添加其他功能,</span> <span class="to-animate">${customer.aid}</span></h1>
				<p class="to-animate">Interested in working together? Email me at <a href="mailto:info@freehtml5.co" class="to-animate">wuwukai@lol.lol</a></p>
			</div>
			<div id="social-animate" class="fh5co-social">
				<ul>
					<li><a href="#" class="to-animate"><i class="icon-twitter-with-circle"></i></a></li>
					<li><a href="#" class="to-animate"><i class="icon-facebook-with-circle"></i></a></li>
					<li><a href="#" class="to-animate"><i class="icon-instagram-with-circle"></i></a></li>
				</ul>
			</div>
			<div class="overlay"></div>
		</div>
        <div class="copyrights">Collect from <a href="http://www.cssmoban.com/" >五五开周边商城</a></div>

		
		
		<!-- jQuery -->
		<script src="js/jquery.min.js"></script>
		<!-- jQuery Easing -->
		<script src="js/jquery.easing.1.3.js"></script>
		<!-- Bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<!-- Waypoints -->
		<script src="js/jquery.waypoints.min.js"></script>
		<!-- Main JS -->
		<script src="js/main.js"></script>

	
	</body>
</html>
