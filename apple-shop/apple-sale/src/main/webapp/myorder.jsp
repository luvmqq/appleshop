<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<!DOCTYPE html>
<html lang="zh">

<head>
<title>Cart</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="aStar Fashion Template Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css"
	href="styles/bootstrap-4.1.3/bootstrap.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css"
	href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css"
	href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css"
	href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/cart.css">
<link rel="stylesheet" type="text/css" href="styles/cart_responsive.css">
<script src="js/jquery-3.2.1.min.js"></script>
</head>

<body>

	<div class="super_container">

		<div id="dd_div">
			<script>
				$("#dd_div").load("sidebar.jsp");
			</script>

		</div>

		<div class="home">
			<div class="parallax_background parallax-window"
				data-parallax="scroll"
				data-image-src="images/product_background.jpg" data-speed="0.8"></div>
			<div class="home_container">
				<div class="home_content">
					<div class="home_title">订单详情</div>
					<div class="breadcrumbs">
						<ul
							class="d-flex flex-row align-items-center justify-content-start">
							<li><a href="index.html">主页</a></li>
							<li>俺的订单详情</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Cart -->

		<div class="cart_section">
			<div class="section_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="cart_container">
								<!-- Cart Bar -->
								<div class="cart_bar">
									<table class="table table-hover">
										<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col">订单号</th>
											<th scope="col">创建时间</th>
											<th scope="col">金额</th>
											<th scope="col">状态</th>
											<th scope="col">操作</th>
										</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>


	<script src="styles/bootstrap-4.1.3/popper.js"></script>
	<script src="styles/bootstrap-4.1.3/bootstrap.min.js"></script>
	<script src="plugins/greensock/TweenMax.min.js"></script>
	<script src="plugins/greensock/TimelineMax.min.js"></script>
	<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="plugins/greensock/animation.gsap.min.js"></script>
	<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="plugins/easing/easing.js"></script>
	<script src="plugins/parallax-js-master/parallax.min.js"></script>

	<script src="zhouranJS/common.js"></script>
	<script src="zhouranJS/ajax.js"></script>
	<script src="js/shopcar.js"></script>
	<script src="zhouranJS/calculator.js"></script>
</body>
<script>

    $(function () {
        let param = {};
        param.pageSize = 10;
        param.pageNum = 1;
        getJSON("selectorder", param, callback_list);
        function callback_list(result) {
            console.log(result);
            //构建表格
            build_body(result);
        }
    });



    //构建表格
    function build_body(result) {
        let content = "";
        $.each(result.extend.pageInfo.list, function (i, obj) {
            let ststus = "";
            let btn = "";
            if(obj.ostatus===1){
                ststus='<span style="color: red" >待支付</span>';
                btn=`<div class="btn btn-sm btn-info" data-id='${obj.oid}'>查看订单</div>`;
                btn+=`<div class="btn btn-sm btn-danger" data-id='${obj.oid}'>去支付</div>`
            }else if(obj.ostatus===2){
                ststus='<span style="color: #0B58A8">已支付</span>';
                btn=`<div class="btn btn-sm btn-info" data-id='${obj.oid}'>查看订单<a href=""</div>`;
            } else if(obj.ostatus===3){
                ststus='<span style="color: #39de21;">已发货</span>';
                btn=`<div class="btn btn-sm btn-info" data-id='${obj.oid}'>查看订单</div>`;
            } else if(obj.ostatus===4){
                ststus='超时失效';
                btn=`<div class="btn btn-sm btn-info" data-id='${obj.oid}'>查看订单</div>`;
            }

            content += " <tr>" +
				"<td>"+i+"</td>" +
				"<td>"+obj.oid+"</td>" +
				"<td>"+obj.odate+"</td>" +
				"<td>￥ "+obj.ototal+"</td>" +
				"<td>"+ststus+"</td>" +
				"<td>"+btn+"</td>" +
				"</tr>";

        });

        $("tbody").empty().append(content);
    }

    //查看订单
    $(document).on("click",".btn-info",function () {
        let id = $(this).data("id"); //订单id
		let ststus = $(this).data("");
        window.location.href="selectid?oid="+id;
    });

    //去支付
    $(document).on("click",".btn-danger",function () {
        let id = $(this).data("id"); //订单id
        window.location.href="apay?oid="+id;
    });




</script>
</html>