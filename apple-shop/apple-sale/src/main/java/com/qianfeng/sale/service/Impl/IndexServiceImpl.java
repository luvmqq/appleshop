package com.qianfeng.sale.service.Impl;

import com.github.pagehelper.PageHelper;
import com.qianfeng.rush.mapper.GoodsMapper;
import com.qianfeng.rush.mapper.GoodsTypeMapper;
import com.qianfeng.rush.pojo.GoodsPojo;
import com.qianfeng.rush.pojo.GoodsTypePojo;
import com.qianfeng.sale.service.IIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexServiceImpl implements IIndexService {

    @Autowired
    GoodsTypeMapper goodsTypeMapper;

    @Autowired
    GoodsMapper goodsMapper;

    public List<GoodsTypePojo> queryGoodsTypes() {
        List<GoodsTypePojo> goodsTypeList = goodsTypeMapper.queryGoodsTypeThree();
        return goodsTypeList;
    }

    public List<GoodsPojo> queryGoods() {
        PageHelper.startPage(1,6);//实现只查询六个
        List<GoodsPojo> list = goodsMapper.queryGoodsByPojo(null);
        return list;
    }
}
