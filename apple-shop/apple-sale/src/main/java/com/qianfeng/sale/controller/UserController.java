package com.qianfeng.sale.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianfeng.rush.pojo.CustomerPojo;
import com.qianfeng.rush.pojo.OrderDetailPojo;
import com.qianfeng.rush.pojo.OrderPojo;
import com.qianfeng.rush.utils.Msg;
import com.qianfeng.sale.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import javax.jws.WebParam;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    ICustomerService customerService;

    @RequestMapping("customerlogin")
    public String  login(String name, String pwd, HttpSession session,Model mv){
        if (name.trim().equals("")||name.trim()==""){
            mv.addAttribute("msg","用户名不可为空。");
            return "login";
        }
        if (pwd.trim().equals("")||pwd.trim()==""){
            mv.addAttribute("msg","密码不可为空。");
            return "login";
        }
        CustomerPojo customerPojo = customerService.foundCustomerPojo(new CustomerPojo(name,pwd));
        if (customerPojo==null){
            mv.addAttribute("msg","登录失败，请确认用户名或密码是否正确！");
            return "login";
        }
        session.setAttribute("customer",customerPojo);
        return "forward:index";
    }

    @RequestMapping("register")
    public String register(String name, String pwd, String repwd, ModelAndView mv){
        if (name.trim().equals("")||name.trim()==""){
            mv.addObject("msg","用户名不可为空。");
            return "register";
        }
        if (pwd.trim().equals("")||pwd.trim()==""){
            mv.addObject("msg","密码不可为空。");
            return "register";
        }
        if (repwd.trim().equals("")||repwd.trim()==""){
            mv.addObject("msg","重复密码不可为空。");
            return "register";
        }
        boolean flg = customerService.addCustomer(new CustomerPojo(name,pwd));
        if (flg==false){
            mv.addObject("msg","添加失败");
            return "register";
        }
        return "login";
    }

    @RequestMapping("selectorder")
    @ResponseBody
    public Msg getDingdan(HttpSession session){
        CustomerPojo customer = (CustomerPojo) session.getAttribute("customer");
        PageHelper.startPage(1,6);
        System.out.println("————————————————————");
        System.out.println(customer.getAid());
        List<OrderPojo> list = customerService.selectdetail(customer.getAid());
        PageInfo pageInfo = new PageInfo(list);
        return Msg.success().add("pageInfo",pageInfo);
    }

    @RequestMapping("logout")
    public String logout(HttpSession session){
        session.removeAttribute("customer");
        return "index";
    }
}
