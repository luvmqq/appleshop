package com.qianfeng.sale.service;

import com.qianfeng.rush.pojo.GoodsPojo;

import java.util.List;

public interface IGoodsService {
    /**
     * 根据条件查询商品信息
     * @param goodsPojo
     * @return
     */
    public List<GoodsPojo> queryGoodsByPojo(GoodsPojo goodsPojo);

    GoodsPojo queryGoodsById(String gid);
}
