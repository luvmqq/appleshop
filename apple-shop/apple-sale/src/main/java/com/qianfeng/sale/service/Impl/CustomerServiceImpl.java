package com.qianfeng.sale.service.Impl;

import com.github.pagehelper.PageHelper;
import com.qianfeng.rush.mapper.CustomerMapper;
import com.qianfeng.rush.pojo.CustomerPojo;
import com.qianfeng.rush.pojo.OrderDetailPojo;
import com.qianfeng.rush.pojo.OrderPojo;
import com.qianfeng.sale.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements ICustomerService {
    @Autowired
    CustomerMapper customerMapper;
    public CustomerPojo foundCustomerPojo(CustomerPojo customerPojo) {
        return customerMapper.foundCustomerPojo(customerPojo);
    }

    public boolean addCustomer(CustomerPojo customerPojo) {
        return customerMapper.addCustomer(customerPojo);
    }

    public List<OrderPojo> selectdetail(int aid) {
        return customerMapper.selectdetail(aid);
    }
}
