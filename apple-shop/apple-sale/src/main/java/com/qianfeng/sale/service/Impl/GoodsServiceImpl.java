package com.qianfeng.sale.service.Impl;

import com.github.pagehelper.PageHelper;
import com.qianfeng.rush.mapper.GoodsMapper;
import com.qianfeng.rush.pojo.GoodsPojo;
import com.qianfeng.sale.service.IGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsServiceImpl implements IGoodsService {

    @Autowired
    GoodsMapper goodsMapper;

    public List<GoodsPojo> queryGoodsByPojo(GoodsPojo goodsPojo) {
        PageHelper.startPage(goodsPojo.getPageNum(),goodsPojo.getPageSize());
        return goodsMapper.queryGoodsByPojo(goodsPojo);
    }

    public GoodsPojo queryGoodsById(String gid) {

        return goodsMapper.queryGoodsById(gid);
    }
}
