package com.qianfeng.sale.controller;

import com.github.pagehelper.PageInfo;
import com.qianfeng.rush.entity.GoodsTypeEntity;
import com.qianfeng.rush.pojo.GoodsPojo;
import com.qianfeng.rush.pojo.GoodsTypePojo;
import com.qianfeng.sale.service.IIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class IndexController {
    @Autowired
    IIndexService indexService;

    @RequestMapping("index")
    public String index(Model model){
        //查询商品类别列表 只查询了三个类别
        List<GoodsTypePojo> list = indexService.queryGoodsTypes();
        //查询六个商品
        List<GoodsPojo> goodsList = indexService.queryGoods();
        PageInfo<GoodsPojo> pageInfo = new PageInfo<GoodsPojo>(goodsList);
        model.addAttribute("goodsTypeList",list);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("goodsList",goodsList);
        return "index";
    }
}
