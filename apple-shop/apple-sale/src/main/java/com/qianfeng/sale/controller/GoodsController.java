package com.qianfeng.sale.controller;

import com.github.pagehelper.PageInfo;
import com.qianfeng.rush.pojo.GoodsPojo;
import com.qianfeng.sale.service.IGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class GoodsController {

    @Autowired
    IGoodsService goodsService;

    @RequestMapping("queryGoods")
    public ModelAndView queryGoods(GoodsPojo goodsPojo){
        ModelAndView mv = new ModelAndView("goodsList");
        //查询商品列表
        List<GoodsPojo> list = goodsService.queryGoodsByPojo(goodsPojo);
        PageInfo<GoodsPojo> pageInfo = new PageInfo<GoodsPojo>(list);
        //将数据添加到页面去展示
        /*mv.addObject("goodsList",list);*/
        mv.addObject("pageInfo",pageInfo);
        mv.addObject("goodsPojo",goodsPojo);
        //返回商品列表
        return mv;
    }

}
