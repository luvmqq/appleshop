package com.qianfeng.sale.service;

import com.qianfeng.rush.pojo.CustomerPojo;
import com.qianfeng.rush.pojo.OrderDetailPojo;
import com.qianfeng.rush.pojo.OrderPojo;

import java.util.List;

public interface ICustomerService {
    public CustomerPojo foundCustomerPojo(CustomerPojo customerPojo);

    public boolean addCustomer(CustomerPojo customerPojo);

    List<OrderPojo> selectdetail(int aid);
}
