package com.qianfeng.sale.service;


import com.qianfeng.rush.entity.GoodsTypeEntity;
import com.qianfeng.rush.pojo.GoodsPojo;
import com.qianfeng.rush.pojo.GoodsTypePojo;

import java.util.List;

public interface IIndexService {
    //首页铲鲟三个商品类别
    public List<GoodsTypePojo> queryGoodsTypes();
    /**
     * 查询6个商品
     * @return
     */
    public List<GoodsPojo> queryGoods();
}
