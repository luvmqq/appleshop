package com.qianfeng.rush.service;

import com.qianfeng.rush.entity.AdminEntity;
import com.qianfeng.rush.pojo.AdminPojo;

import java.util.List;

public interface IAdminService {

    AdminPojo login(AdminEntity adminEntity);

    List<AdminPojo> getAdList(AdminEntity adminEntity);

    boolean delAdmins(String[] ids);

    boolean addAdmin(AdminEntity adminEntity);

    void deleteById(int aid);

    boolean updateAdmin(AdminEntity adminEntity);
}
