package com.qianfeng.rush.service;

import com.qianfeng.rush.entity.RoleEntity;
import com.qianfeng.rush.pojo.RolePojo;
import org.springframework.stereotype.Service;

import java.util.List;


public interface IRoleService {

    /**
     * 根据条件查询所有的角色
     * @param roleEntity
     * @return
     */
    public List<RolePojo> queryRoles(RoleEntity roleEntity);
}
