package com.qianfeng.rush.service.Impl;

import com.github.pagehelper.PageHelper;
import com.qianfeng.rush.entity.RoleEntity;
import com.qianfeng.rush.mapper.RoleMapper;
import com.qianfeng.rush.pojo.RolePojo;
import com.qianfeng.rush.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements IRoleService {
    @Autowired
    RoleMapper roleMapper;
    public List<RolePojo> queryRoles(RoleEntity roleEntity) {
        if (null != roleEntity){
            PageHelper.startPage(roleEntity.getPageNum(),roleEntity.getPageSize());
        }
        return roleMapper.queryRoles(roleEntity);
    }
}
