package com.qianfeng.rush.service.Impl;

import com.github.pagehelper.PageHelper;
import com.qianfeng.rush.entity.AdminEntity;
import com.qianfeng.rush.mapper.AdminMapper;
import com.qianfeng.rush.pojo.AdminPojo;
import com.qianfeng.rush.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements IAdminService {
    @Autowired
    private AdminMapper adminMapper;
    public AdminPojo login(AdminEntity adminEntity) {

        return adminMapper.loginQueryAuth(adminEntity);

    }

    public List<AdminPojo> getAdList(AdminEntity adminEntity) {
        //分页
        PageHelper.startPage(adminEntity.getPageNum(),adminEntity.getPageSize());

        return adminMapper.getAdList(adminEntity);

    }

    public boolean delAdmins(String[] ids) {

        return adminMapper.delAdmins(ids);
    }

    public boolean addAdmin(AdminEntity adminEntity) {
        //1:添加用户，主键回填
        adminMapper.addAdmin(adminEntity);
        //2:给用户绑定角色
        boolean b1 = adminMapper.bindRoles(adminEntity.getAid(),adminEntity.getRoleids());
        return b1;
    }

    public void deleteById(int aid) {
        adminMapper.deleteById(aid);
    }

    public boolean updateAdmin(AdminEntity adminEntity) {
        adminMapper.updateAdmin(adminEntity);
        boolean b1 = adminMapper.bindRoles(adminEntity.getAid(),adminEntity.getRoleids());
        return b1;
    }
}
