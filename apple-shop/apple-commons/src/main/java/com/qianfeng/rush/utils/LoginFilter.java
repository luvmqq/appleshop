package com.qianfeng.rush.utils;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class LoginFilter implements Filter {
	private static String[] paths; // Ҫ���е���Դ��ַ

	public LoginFilter() {

	}

	public void destroy() {
		// TODO Auto-generated method stub
	}

	// ����������Ӧ
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		// ת��ԭ��̬���������Ӧ
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		// Ԥ�ȷ�����Դ:paths�а�����URL·��
		String path = request.getServletPath();// �õ�������Դ��URI·��
		Logger.getLogger("权限过滤器").debug("path:" + path);
		for (int i = 0; i < paths.length; i++) {
			if (path.startsWith(paths[i])) { // �����ǰ�����URI·��������Ҫ���е���Դ��ͷ�������
				// ��������
				chain.doFilter(request, response);
				// һ�����и���Դ�󣬷���,��Ϊ��������Filter��doFilter���������ض���
				return;
			}
		}

		// �û���¼�ɹ���session�л���һ��userMap
		Map userMap = (Map)request.getSession().getAttribute("userMap");
		if(userMap==null) {//���userMap==null,֤���û�û�е�¼�ɹ��������غ��ض���login.html�����¼
			request.getSession().setAttribute("error", "对不起，您没有权限访问，请登录！");
			//request.getContextPath():��ȡ����/EducationSystem
			response.sendRedirect(request.getContextPath()+"/login.html");
			return;//����
		}

		//��¼�ɹ�������У����Է���������Դ
		chain.doFilter(request, response);
	}

	// �������ĳ�ʼ�������ڷ�����������ʱ��ͱ�������
	public void init(FilterConfig fConfig) throws ServletException {
		// ��ȡFilter�ĳ�ʼ������
		String initParameter = fConfig.getInitParameter("letgo");
		paths = initParameter.split(";");
	}

}
