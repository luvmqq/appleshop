package com.qianfeng.rush.mapper;

import com.qianfeng.rush.entity.RoleEntity;
import com.qianfeng.rush.pojo.RolePojo;

import java.util.List;

public interface RoleMapper {

    /**
     * 根据条件查询所有角色
     */
    public List<RolePojo> queryRoles(RoleEntity roleEntity);
}
