package com.qianfeng.rush.mapper;

import com.qianfeng.rush.entity.AdminEntity;
import com.qianfeng.rush.pojo.AdminPojo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminMapper {
    /**
     * 管理员登录
     * @param adminEntity
     * @return
     */
    AdminPojo login(AdminEntity adminEntity);

    /**
     * 根据条件分页查询管理员列表
     * @param adminEntity
     * @return
     */
    List<AdminPojo> getAdList(AdminEntity adminEntity);

    /**
     * 根据用户名密码。级联查询用户角色及权限
     * @param adminEntity
     * @return
     */
    public AdminPojo loginQueryAuth(AdminEntity adminEntity);

    /**
     * 根据用户Id，查询其所有角色和权限
     * @param aid
     * @return
     */
    public AdminPojo queryAuthById(int aid);

    boolean delAdmins(String[] ids);

    int addAdmin(AdminEntity adminEntity);

    boolean bindRoles(@Param("aid") int aid,@Param("roles") int[] roles);

    void deleteById(int aid);

    void updateAdmin(AdminEntity adminEntity);
}
