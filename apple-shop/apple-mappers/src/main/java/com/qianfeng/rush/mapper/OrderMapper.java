package com.qianfeng.rush.mapper;

import com.qianfeng.rush.pojo.OrderDetailPojo;
import com.qianfeng.rush.pojo.OrderPojo;

import java.util.List;

public interface OrderMapper {
    /**
     * 创建订单信息
     * @param orderPojo
     * @return
     */
    public boolean createOrder(OrderPojo orderPojo);

    /**
     * 创建订单详情
     * @param detais
     * @return
     */
    public boolean createOrderDetails(List<OrderDetailPojo> detais);

    /**
     *取消目标订单
     * @param oid
     * @return
     */
    public boolean cancelOrder(String oid);

    /**
     * 支付成功的回调方法
     * @param oid
     * @return
     */
    public boolean apaySuccess(String oid);

    public List<OrderPojo> selectById(String oid);
}

