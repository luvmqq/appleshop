package com.qianfeng.rush.mapper;

import com.qianfeng.rush.pojo.GoodsPojo;

import java.util.List;

public interface GoodsMapper {
    /**
     * 查询商品：根据各种各样的条件进行查询
     * 1：根据类别：衣服裤子
     * 2：价格
     * 3：glabel 商品销售类别
     * 4：商品的类别（男 女 童）
     * @param goodsPojo
     * @return
     */
    public List<GoodsPojo> queryGoodsByPojo(GoodsPojo goodsPojo);

    GoodsPojo queryGoodsById(String gid);
}
