package com.qianfeng.rush.mapper;

import com.qianfeng.rush.pojo.GoodsTypePojo;

import java.util.List;

public interface GoodsTypeMapper {
    public List<GoodsTypePojo> queryGoodsTypeThree();
}
