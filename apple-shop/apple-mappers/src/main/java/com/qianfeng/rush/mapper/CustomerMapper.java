package com.qianfeng.rush.mapper;

import com.qianfeng.rush.pojo.CustomerPojo;
import com.qianfeng.rush.pojo.OrderDetailPojo;
import com.qianfeng.rush.pojo.OrderPojo;

import java.util.List;

public interface CustomerMapper {

    public CustomerPojo foundCustomerPojo(CustomerPojo customerPojo);

    public boolean addCustomer(CustomerPojo customerPojo);

    public List<OrderPojo> selectdetail(int aid);
}
