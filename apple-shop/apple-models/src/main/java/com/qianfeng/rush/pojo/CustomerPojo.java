package com.qianfeng.rush.pojo;

public class CustomerPojo {
    private int aid;
    private String aname;
    private String apass;

    public CustomerPojo() {
    }

    public CustomerPojo(String aname, String apass) {
        this.aname = aname;
        this.apass = apass;
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getApass() {
        return apass;
    }

    public void setApass(String apass) {
        this.apass = apass;
    }

    @Override
    public String toString() {
        return "CustomerPojo{" +
                "aid=" + aid +
                ", aname='" + aname + '\'' +
                ", apass='" + apass + '\'' +
                '}';
    }
}
