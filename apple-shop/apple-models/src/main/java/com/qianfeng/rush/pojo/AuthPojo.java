package com.qianfeng.rush.pojo;

public class AuthPojo {
    private int auid;//权限id
    private String auname;//权限名称
    private String aupath;//路径
    private short autype;//类型
    private int aupid;//权限的父id

    public AuthPojo() {

    }

    public AuthPojo(String auname, String aupath, short autype, int aupid) {
        this.auname = auname;
        this.aupath = aupath;
        this.autype = autype;
        this.aupid = aupid;
    }

    @Override
    public String toString() {
        return "AuthPojo{" +
                "auid=" + auid +
                ", auname='" + auname + '\'' +
                ", aupath='" + aupath + '\'' +
                ", autype=" + autype +
                ", aupid=" + aupid +
                '}';
    }

    public int getAuid() {
        return auid;
    }

    public void setAuid(int auid) {
        this.auid = auid;
    }

    public String getAuname() {
        return auname;
    }

    public void setAuname(String auname) {
        this.auname = auname;
    }

    public String getAupath() {
        return aupath;
    }

    public void setAupath(String aupath) {
        this.aupath = aupath;
    }

    public short getAutype() {
        return autype;
    }

    public void setAutype(short autype) {
        this.autype = autype;
    }

    public int getAupid() {
        return aupid;
    }

    public void setAupid(int aupid) {
        this.aupid = aupid;
    }
}
